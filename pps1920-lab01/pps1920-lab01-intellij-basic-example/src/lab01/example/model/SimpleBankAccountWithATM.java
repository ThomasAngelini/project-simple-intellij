package lab01.example.model;

public class SimpleBankAccountWithATM implements BankAccount {

    private double balance;
    private final AccountHolder holder;
    private final ATMStrategy atmStrategy;

    public SimpleBankAccountWithATM(final AccountHolder holder, final double balance, ATMStrategy atmStrategy) {
        this.holder = holder;
        this.balance = balance;
        this.atmStrategy = atmStrategy;
    }

    @Override
    public AccountHolder getHolder() {
        return this.holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void deposit(int usrID, double amount) {
        if (checkUser(usrID) && isDepositAllowed(amount)){
            this.balance += amount;
            this.updateBalanceWithATM();
        }
    }

    @Override
    public void withdraw(int usrID, double amount) {
        if (checkUser(usrID) && isWithdrawAllowed(amount)) {
            this.balance -= amount;
            this.updateBalanceWithATM();
        }
    }

    private double applyATMStrategy(final double balance){return this.atmStrategy.applyATMStrategy(this.balance);}

    private boolean isWithdrawAllowed(final double amount){
        return this.applyATMStrategy(this.balance) >= amount;
    }

    private boolean isDepositAllowed(final double amount){
        return amount + this.applyATMStrategy(balance) >= 0;
    }

    private boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }

    private void updateBalanceWithATM(){
        this.balance = this.applyATMStrategy(this.balance);
    }
}
