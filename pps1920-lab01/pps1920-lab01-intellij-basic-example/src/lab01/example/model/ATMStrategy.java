package lab01.example.model;

@FunctionalInterface
public interface ATMStrategy {
    public double applyATMStrategy(double balance);
}
