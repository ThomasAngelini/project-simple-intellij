import lab01.example.model.ATMStrategy;
import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;

import lab01.example.model.SimpleBankAccountWithATM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleBankAccountWithATMTest {


        private AccountHolder accountHolder;
        private BankAccount bankAccount;
        private ATMStrategy atmStrategy;
        private final double ATMFee = 1;

        @BeforeEach
        void beforeEach(){
            accountHolder = new AccountHolder("Mario", "Rossi", 1);
            this.atmStrategy = (balance) -> balance - this.ATMFee;
            bankAccount = new SimpleBankAccountWithATM(accountHolder, 0, atmStrategy);
        }

        @Test
        void testDeposit() {
            this.bankAccount.deposit(1,30);
            assertEquals(29, bankAccount.getBalance());
        }

        @Test
        void testErroneusDeposit(){
            this.bankAccount.deposit(1,0.5);
            assertEquals(0,bankAccount.getBalance());
        }

        @Test
        void testWithdraw(){
            this.bankAccount.deposit(1,30);
            this.bankAccount.withdraw(1,10);
            assertEquals(18,bankAccount.getBalance());
        }

        @Test
        void testErroneusWithdraw(){
            this.bankAccount.deposit(1,30);
            this.bankAccount.withdraw(1,30);
            assertEquals(29,bankAccount.getBalance());
        }

}
