package lab01.tdd;

import java.util.Optional;
import java.util.stream.IntStream;

public class MultipleOfStrategy implements SelectStrategy {

    private final CircularList circularList;

    public MultipleOfStrategy(final CircularList circularList){
        this.circularList = circularList;
    }

    @Override
    public boolean apply(int element) {
        return this.circularList.isEmpty() ? false : searchElement(element);
    }

    private boolean searchElement(int element){
        boolean finded = false;

        for (int i = 0; i <  this.circularList.size()-1; i++) {
            if (Math.floorMod(this.circularList.next().get(),element) == 0){
                finded = true;
                this.circularList.previous();
                break;
            }
        }
        return finded;
    }
}
