package lab01.tdd;

import java.util.Optional;

public class EqualsStrategy implements SelectStrategy {

    private final CircularList circularList;

    public EqualsStrategy(final CircularList circularList){
        this.circularList=circularList;
    }

    @Override
    public boolean apply(int element) {
        return this.circularList.isEmpty() ? false : equalNumber(element);
    }
    
    private boolean equalNumber(int element){
        boolean finded = false;

        for (int i = 0; i < this.circularList.size()-1; i++) {
            if (this.circularList.next().get() == element){
                finded = true;
                this.circularList.previous();
                break;
            }
        }

        return finded;
    }
        
    
}
