import lab01.tdd.*;

import org.junit.jupiter.api.*;

import javax.swing.text.html.Option;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList circularList;
    private SelectStrategy evenStrategy;
    private SelectStrategy multipleStrategy;
    private SelectStrategy equalStrategy;

    @BeforeEach
    void arrange(){
        this.circularList = new SimpleCircularList();
        this.evenStrategy = SimpleStrategyFactory.createEvenStrategy(circularList);
        this.multipleStrategy = SimpleStrategyFactory.createMultipleStrategy(circularList);
        this.equalStrategy = SimpleStrategyFactory.createEqualStrategy(circularList);
    }


    @Test
    void testInitialized(){
        assertEquals(0,this.circularList.size());
    }

    @Test
    void testAdd(){
        this.circularList.add(3);
        assertEquals(1,this.circularList.size());
    }

    @Test
    void testIsEmptyBeforeAdd(){
        assertEquals(true,this.circularList.isEmpty());
    }

    @Test
    void testIsEmptyAfterAdd(){
        this.circularList.add(1);
        assertEquals(false,this.circularList.isEmpty());
    }

    @Test
    void testNextIfEmpty(){
        assertEquals(Optional.empty(),this.circularList.next());
    }

    @Test
    void testNextOneValue(){
        this.circularList.add(1);
        assertEquals(Optional.of(1),this.circularList.next());
    }

    @Test
    void testNextOneValueTwice(){
        this.circularList.add(1);
        this.circularList.add(1);
        assertEquals(Optional.of(1),this.circularList.next());
    }

    @Test
    void testNextTwoValueOnce(){
        this.circularList.add(1);
        this.circularList.add(2);
        assertEquals(Optional.of(1),this.circularList.next());
    }

    @Test
    void testNextTwoValueTwice(){
        this.circularList.add(1);
        this.circularList.add(2);
        assertEquals(Optional.of(1),this.circularList.next());
        assertEquals(Optional.of(2),this.circularList.next());
    }

    @Test
    void testPreviousEmpty(){
        assertEquals(Optional.empty(), this.circularList.previous());
    }

    @Test
    void testPreviousOneValue(){
        this.circularList.add(1);
        assertEquals(Optional.of(1),this.circularList.previous());
    }

    @Test
    void testPreviousTwoValueOnce(){
        this.circularList.add(1);
        this.circularList.add(2);
        assertEquals(Optional.of(1),this.circularList.next());
    }

    @Test
    void testPreviousTwoValueTwice() {
        this.circularList.add(1);
        this.circularList.add(2);
        assertEquals(Optional.of(1), this.circularList.next());
        assertEquals(Optional.of(2), this.circularList.next());
    }

    @Test
    void testNextAndPrevious(){
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        assertEquals(Optional.of(1),this.circularList.next());
        assertEquals(Optional.of(2),this.circularList.previous());
        assertEquals(Optional.of(1),this.circularList.previous());
        assertEquals(Optional.of(3),this.circularList.next());
        assertEquals(Optional.of(1),this.circularList.next());
    }

    @Test
    void testResetInitialize(){
        this.circularList.add(1);
        this.circularList.reset();
        assertEquals(Optional.of(1),this.circularList.next());
    }

    @Test
    void testResetComplete(){
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        this.circularList.next();
        this.circularList.next();
        this.circularList.reset();
        assertEquals(Optional.of(1),this.circularList.previous());
    }

    @Test
    void testEvenStrategyInit(){
        assertEquals(Optional.empty(),this.circularList.next(evenStrategy));
    }

    @Test
    void testEvenStrategy(){
        this.circularList.add(0);
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        this.circularList.add(4);
        assertEquals(Optional.of(2),this.circularList.next(evenStrategy));
    }

    @Test
    void testMultipleStrategy(){
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        this.circularList.add(4);
        assertEquals(Optional.of(2),this.circularList.next(multipleStrategy));
    }



    @Test
    void testMultipleStrategyFalse(){
        this.circularList.add(1);
        this.circularList.add(3);
        assertEquals(Optional.empty(),this.circularList.next(multipleStrategy));
    }

    @Test
    void testMultipleStrategyComplex(){
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        this.circularList.add(4);

        this.circularList.next();
        this.circularList.next();

        assertEquals(Optional.of(4),this.circularList.next(multipleStrategy));
    }

    @Test
    void testEqualStrategy(){
        this.circularList.add(1);
        this.circularList.add(2);
        this.circularList.add(3);
        this.circularList.add(4);
        this.circularList.add(2);
        assertEquals(Optional.of(2),this.circularList.next(equalStrategy));
    }

    @Test
    void testEqualStrategyFalse(){
        this.circularList.add(1);
        this.circularList.add(3);
        this.circularList.add(4);
        assertEquals(Optional.empty(),this.circularList.next(equalStrategy));
    }

}
